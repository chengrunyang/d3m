import json
import logging
import os
import sys
import typing
import unittest

from d3m import container, exceptions, index, utils
from d3m.metadata import base as metadata_base, hyperparams, params, pipeline
from d3m.primitive_interfaces import base, transformer, supervised_learning

TEST_PRIMITIVES_DIR = os.path.join(os.path.dirname(__file__), 'data', 'primitives')

sys.path.insert(0, TEST_PRIMITIVES_DIR)

from test_primitives.monomial import MonomialPrimitive
from test_primitives.random import RandomPrimitive
from test_primitives.sum import SumPrimitive
from test_primitives.increment import IncrementPrimitive


TEST_PIPELINE_1 = """
{
  "id": "2b50a7db-c5e2-434c-b02d-9e595bd56788",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
  "source": {
    "name": "Test author",
    "contact": "mailto:test@example.com"
  },
  "created": "2018-02-28T09:42:27.443844Z",
  "context": "TESTING",
  "name": "Test pipeline",
  "description": "Just a test pipeline",
  "users": [
    {
      "id": "f32467bc-698c-4ab6-a489-2e8f73fcfdaa",
      "reason": "User was making a test",
      "rationale": "I made a test"
    }
  ],
  "inputs": [
    {
      "name": "dataframe inputs"
    },
    {
      "name": "dataframe outputs"
    },
    {
      "name": "extra data"
    }
  ],
  "outputs": [
    {
      "name": "dataframe predictions",
      "data": "steps.6.main"
    }
  ],
  "steps": [
    {
      "type": "PRIMITIVE",
      "primitive": {
        "id": "efa24fae-49c4-4482-b49f-ceb351c0d916",
        "version": "0.1.0",
        "python_path": "d3m.primitives.test.LossPrimitive",
        "name": "Loss Primitive"
      },
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        },
        "outputs": {
          "type": "CONTAINER",
          "data": "inputs.1"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ]
    },
    {
      "type": "PRIMITIVE",
      "primitive": {
        "id": "00c3a435-a87c-405b-bed9-3a8c402d4431",
        "version": "0.1.0",
        "python_path": "d3m.primitives.test.Model1Primitive",
        "name": "Model 1 Primitive"
      },
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        },
        "outputs": {
          "type": "CONTAINER",
          "data": "inputs.1"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ]
    },
    {
      "type": "PRIMITIVE",
      "primitive": {
        "id": "4987c4b0-cf4c-4f7f-9bcc-557a6d72589d",
        "version": "0.1.0",
        "python_path": "d3m.primitives.test.Model2Primitive",
        "name": "Model 2 Primitive"
      },
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        },
        "outputs": {
          "type": "CONTAINER",
          "data": "inputs.1"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ]
    },
    {
      "type": "PRIMITIVE",
      "primitive": {
        "id": "9c00d42d-382d-4177-a0e7-082da88a29c8",
        "version": "0.1.0",
        "python_path": "d3m.primitives.test.SumPrimitive",
        "name": "Sum Values",
        "digest": "__SUM_DIGEST__"
      },
      "arguments": {
        "inputs": {
            "type": "CONTAINER",
            "data": "inputs.0"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ]
    },
    {
      "type": "PRIMITIVE",
      "primitive": {
        "id": "e42e6f17-77cc-4611-8cca-bba36a46e806",
        "version": "0.1.0",
        "python_path": "d3m.primitives.test.PipelineTestPrimitive",
        "name": "Pipeline Test Primitive"
      },
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        },
        "extra_data": {
          "type": "CONTAINER",
          "data": "inputs.2"
        },
        "offset": {
          "type": "DATA",
          "data": "steps.3.produce"
        }
      },
      "outputs": [
        {
          "id": "produce"
        },
        {
          "id": "produce_score"
        }
      ],
      "hyperparams": {
        "loss": {
          "type": "PRIMITIVE",
          "data": 0
        },
        "column_to_operate_on": {
          "type": "VALUE",
          "data": 5
        },
        "ensemble": {
          "type": "PRIMITIVE",
          "data": [
            1,
            2
          ]
        },
        "columns_to_operate_on": {
          "type": "VALUE",
          "data": [3, 6, 7]
        }
      },
      "users": [
        {
          "id": "98e5cc4a-7edc-41a3-ac98-ee799fb6a41b",
          "reason": "User clicked on a button",
          "rationale": "I dragged an icon"
        }
      ]
    },
    {
      "type": "SUBPIPELINE",
      "pipeline": {
        "id": "0113b91f-3010-4a47-bd56-a50c4e28a4a4"
      },
      "inputs": [
        {
          "data": "steps.4.produce"
        }
      ],
      "outputs": [
        {
          "id": "pipeline_output"
        }
      ]
    },
    {
      "type": "PLACEHOLDER",
      "inputs": [
        {
          "data": "steps.5.pipeline_output"
        },
        {
          "data": "steps.4.produce_score"
        }        
      ],
      "outputs": [
        {
          "id": "main"
        }
      ]
    }
  ]
}
""".replace('__SUM_DIGEST__', SumPrimitive.metadata.query()['digest'])

TEST_PIPELINE_2 = """
{
  "id": "0113b91f-3010-4a47-bd56-a50c4e28a4a4",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
  "created": "2018-02-28T09:42:27.443844Z",
  "context": "TESTING",
  "name": "Test pipeline",
  "description": "Just a test pipeline",
  "inputs": [
    {}
  ],
  "outputs": [
    {
      "data": "steps.0.produce"
    }
  ],
  "steps": [
    {
      "type": "PRIMITIVE",
      "primitive": {
        "id": "4987c4b0-cf4c-4f7f-9bcc-557a6d72589d",
        "version": "0.1.0",
        "python_path": "d3m.primitives.test.Model2Primitive",
        "name": "Model 2 Primitive"
      },
      "arguments": {
        "inputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        },
        "outputs": {
          "type": "CONTAINER",
          "data": "inputs.0"
        }
      },
      "outputs": [
        {
          "id": "produce"
        }
      ]
    }
  ]
}
"""


class Resolver(pipeline.Resolver):
    def _get_primitive(self, primitive_description: typing.Dict) -> typing.Optional[typing.Type[base.PrimitiveBase]]:
        # To hide any logging or stdout output.
        with utils.silence():
            return super()._get_primitive(primitive_description)

    def get_pipeline(self, pipeline_id):
        if pipeline_id == '0113b91f-3010-4a47-bd56-a50c4e28a4a4':
            return pipeline.Pipeline.from_json(TEST_PIPELINE_2, resolver=self)

        return super().get_pipeline(pipeline_id)


class TestPipeline(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        Inputs = container.DataFrame
        Outputs = container.DataFrame

        class Hyperparams(hyperparams.Hyperparams):
            pass

        class Params(params.Params):
            pass

        class LossPrimitive(supervised_learning.SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
            metadata = metadata_base.PrimitiveMetadata({
                'id': 'efa24fae-49c4-4482-b49f-ceb351c0d916',
                'version': '0.1.0',
                'name': "Loss Primitive",
                'python_path': 'd3m.primitives.test.LossPrimitive',
                'algorithm_types': [
                    metadata_base.PrimitiveAlgorithmType.CROSS_ENTROPY,
                ],
                'primitive_family': metadata_base.PrimitiveFamily.LOSS_FUNCTION
            })

            def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                pass

            def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
                pass

            def fit(self, *, timeout: float = None, iterations: int = None) -> base.CallResult[None]:
                pass

            def get_params(self) -> Params:
                pass

            def set_params(self, *, params: Params) -> None:
                pass

        class Model1Primitive(supervised_learning.SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
            metadata = metadata_base.PrimitiveMetadata({
                'id': '00c3a435-a87c-405b-bed9-3a8c402d4431',
                'version': '0.1.0',
                'name': "Model 1 Primitive",
                'python_path': 'd3m.primitives.test.Model1Primitive',
                'algorithm_types': [
                    metadata_base.PrimitiveAlgorithmType.RANDOM_FOREST,
                ],
                'primitive_family': metadata_base.PrimitiveFamily.CLASSIFICATION
            })

            def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                pass

            def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
                pass

            def fit(self, *, timeout: float = None, iterations: int = None) -> base.CallResult[None]:
                pass

            def get_params(self) -> Params:
                pass

            def set_params(self, *, params: Params) -> None:
                pass

        class Model2Hyperparams(hyperparams.Hyperparams):
            # To test that a primitive instance can be a default value.
            base_estimator = hyperparams.Hyperparameter[base.PrimitiveBase](
                default=LossPrimitive(hyperparams=Hyperparams.defaults()),
                semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
            )

        class Model2Primitive(supervised_learning.SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Model2Hyperparams]):
            metadata = metadata_base.PrimitiveMetadata({
                'id': '4987c4b0-cf4c-4f7f-9bcc-557a6d72589d',
                'version': '0.1.0',
                'name': "Model 2 Primitive",
                'python_path': 'd3m.primitives.test.Model2Primitive',
                'algorithm_types': [
                    metadata_base.PrimitiveAlgorithmType.SUPPORT_VECTOR_MACHINE,
                ],
                'primitive_family': metadata_base.PrimitiveFamily.CLASSIFICATION
            })

            def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                pass

            def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
                pass

            def fit(self, *, timeout: float = None, iterations: int = None) -> base.CallResult[None]:
                pass

            def get_params(self) -> Params:
                pass

            def set_params(self, *, params: Params) -> None:
                pass

        column_index = hyperparams.Hyperparameter[int](-1)

        class PipelineTestHyperparams(hyperparams.Hyperparams):
            loss = hyperparams.Hyperparameter[typing.Optional[base.PrimitiveBase]](default=None, semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'])
            column_to_operate_on = hyperparams.Hyperparameter[int](default=-1, semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'])
            ensemble = hyperparams.Set(elements=hyperparams.Hyperparameter[base.PrimitiveBase](default=MonomialPrimitive), default=(), max_size=10, semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'])
            columns_to_operate_on = hyperparams.Set(column_index, (), 0, None, semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'])

        PipelineTestInputs = typing.Union[container.Dataset, container.DataFrame]

        class PipelineTestPrimitive(transformer.TransformerPrimitiveBase[PipelineTestInputs, Outputs, PipelineTestHyperparams]):
            metadata = metadata_base.PrimitiveMetadata({
                'id': 'e42e6f17-77cc-4611-8cca-bba36a46e806',
                'version': '0.1.0',
                'name': "Pipeline Test Primitive",
                'python_path': 'd3m.primitives.test.PipelineTestPrimitive',
                'algorithm_types': [
                    metadata_base.PrimitiveAlgorithmType.CROSS_ENTROPY,
                ],
                'primitive_family': metadata_base.PrimitiveFamily.LOSS_FUNCTION
            })

            def produce(self, *, inputs: PipelineTestInputs, extra_data: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                pass

            def produce_score(self, *, inputs: PipelineTestInputs, offset: float, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                pass

            def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: PipelineTestInputs, extra_data: Inputs, offset: float, timeout: float = None, iterations: int = None) -> base.MultiCallResult:
                return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, extra_data=extra_data, offset=offset)

        class SimplePipelineTestPrimitive(transformer.TransformerPrimitiveBase[PipelineTestInputs, Outputs, PipelineTestHyperparams]):
            metadata = metadata_base.PrimitiveMetadata({
                'id': '02d966d6-4e4f-465b-ad93-83b14c7c47be',
                'version': '0.1.0',
                'name': "Simple Pipeline Test Primitive",
                'python_path': 'd3m.primitives.test.SimplePipelineTestPrimitive',
                'algorithm_types': [
                    metadata_base.PrimitiveAlgorithmType.CROSS_ENTROPY,
                ],
                'primitive_family': metadata_base.PrimitiveFamily.LOSS_FUNCTION
            })

            def produce(self, *, inputs: PipelineTestInputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                pass

        ColumnsInputs = container.Dataset
        ColumnsOutputs = container.List

        class ColumnSelectionPrimitive(transformer.TransformerPrimitiveBase[ColumnsInputs, ColumnsOutputs, Hyperparams]):
            metadata = metadata_base.PrimitiveMetadata({
                'id': 'fdabb0c2-0555-4188-8f08-eeda722e1f04',
                'version': '0.1.0',
                'name': "Column Selection Primitive",
                'python_path': 'd3m.primitives.test.ColumnSelectionPrimitive',
                'algorithm_types': [
                    metadata_base.PrimitiveAlgorithmType.PRINCIPAL_COMPONENT_ANALYSIS,
                ],
                'primitive_family': metadata_base.PrimitiveFamily.FEATURE_SELECTION
            })

            def produce(self, *, inputs: ColumnsInputs, timeout: float = None, iterations: int = None) -> base.CallResult[ColumnsOutputs]:
                pass

        # To hide any logging or stdout output.
        with cls.assertLogs(cls, level=logging.DEBUG) as cm:
            with utils.redirect_to_logging():
                index.register_primitive('d3m.primitives.test.MonomialPrimitive', MonomialPrimitive)
                index.register_primitive('d3m.primitives.test.RandomPrimitive', RandomPrimitive)
                index.register_primitive('d3m.primitives.test.SumPrimitive', SumPrimitive)
                index.register_primitive('d3m.primitives.test.IncrementPrimitive', IncrementPrimitive)
                index.register_primitive('d3m.primitives.test.LossPrimitive', LossPrimitive)
                index.register_primitive('d3m.primitives.test.Model1Primitive', Model1Primitive)
                index.register_primitive('d3m.primitives.test.Model2Primitive', Model2Primitive)
                index.register_primitive('d3m.primitives.test.PipelineTestPrimitive', PipelineTestPrimitive)
                index.register_primitive('d3m.primitives.test.SimplePipelineTestPrimitive', SimplePipelineTestPrimitive)
                index.register_primitive('d3m.primitives.test.ColumnSelectionPrimitive', ColumnSelectionPrimitive)

                # Just to log something, otherwise "assertLogs" can fail.
                logging.getLogger().debug("Start test.")

    def test_basic(self):
        p = pipeline.Pipeline.from_json(TEST_PIPELINE_1, resolver=Resolver())

        p.check(allow_placeholders=True, input_types={'inputs.0': container.DataFrame, 'inputs.1': container.DataFrame, 'inputs.2': container.DataFrame})

        with self.assertRaisesRegex(exceptions.InvalidPipelineError, 'Step .* of pipeline \'.*\' is a placeholder but there should be no placeholders'):
            p.check(allow_placeholders=False, input_types={'inputs.0': container.DataFrame, 'inputs.1': container.DataFrame, 'inputs.2': container.DataFrame})

        p_json_input = json.loads(TEST_PIPELINE_1)
        p_json_output = p.to_json_structure()

        self.assertEqual(p_json_input, p_json_output)

        self.assertEqual(p_json_input, pipeline.Pipeline.from_json(p.to_json(), resolver=Resolver()).to_json_structure())

        self.assertEqual(p_json_input, pipeline.Pipeline.from_yaml(p.to_yaml(), resolver=Resolver()).to_json_structure())

    def test_primitive_annotation(self):
        # This test does not really belong here but it is easiest to make it here.
        # Test that hyper-parameter can have a primitive instance as a default value
        # and that such primitive can have its metadata converted to JSON.

        self.assertEqual(index.get_primitive('d3m.primitives.test.Model2Primitive').metadata.to_json_structure()['primitive_code']['hyperparams']['base_estimator'], {
            'type': 'd3m.metadata.hyperparams.Hyperparameter',
            'default': 'd3m.primitives.test.LossPrimitive(hyperparams=Hyperparams({}), random_seed=0)',
            'structural_type': 'd3m.primitive_interfaces.base.PrimitiveBase',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        })

    def test_invalid_data_reference(self):
        with self.assertRaisesRegex(exceptions.InvalidArgumentTypeError, 'Cannot add step .*'):
            pipeline.Pipeline.from_json("""
                {
                  "id": "c12a8de1-d4d7-4d4b-b51f-66488e1adcc6",
                  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
                  "created": "2018-02-28T09:42:27.443844Z",
                  "context": "TESTING",
                  "name": "Test pipeline",
                  "description": "Just a test pipeline",
                  "inputs": [
                    {}
                  ],
                  "outputs": [
                    {
                      "data": "steps.0.produce"
                    }
                  ],
                  "steps": [
                    {
                      "type": "PRIMITIVE",
                      "primitive": {
                        "id": "9c00d42d-382d-4177-a0e7-082da88a29c8",
                        "version": "0.1.0",
                        "python_path": "d3m.primitives.test.SumPrimitive",
                        "name": "Sum Values",
                        "digest": "__SUM_DIGEST__"
                      },
                      "arguments": {
                        "inputs": {
                          "type": "CONTAINER",
                          "data": "inputs.1"
                        }
                      },
                      "outputs": [
                        {
                          "id": "produce"
                        }
                      ]
                    }
                  ]
                }
            """.replace('__SUM_DIGEST__', SumPrimitive.metadata.query()['digest']), resolver=Resolver())

    def test_list_of_columns(self):
        pipeline.Pipeline.from_json("""
            {
              "id": "48fa0619-53f2-4a36-8a90-31ba8e08df02",
              "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
              "created": "2018-02-28T09:42:27.443844Z",
              "context": "TESTING",
              "name": "Test pipeline",
              "description": "Just a test pipeline",
              "inputs": [
                {}
              ],
              "outputs": [
                {
                  "data": "steps.1.produce"
                }
              ],
              "steps": [
                {
                  "type": "PRIMITIVE",
                  "primitive": {
                    "id": "fdabb0c2-0555-4188-8f08-eeda722e1f04",
                    "version": "0.1.0",
                    "python_path": "d3m.primitives.test.ColumnSelectionPrimitive",
                    "name": "Column Selection Primitive"
                  },
                  "arguments": {
                    "inputs": {
                      "type": "CONTAINER",
                      "data": "inputs.0"
                    }
                  },
                  "outputs": [
                    {
                      "id": "produce"
                    }
                  ]
                },
                {
                  "type": "PRIMITIVE",
                  "primitive": {
                    "id": "02d966d6-4e4f-465b-ad93-83b14c7c47be",
                    "version": "0.1.0",
                    "python_path": "d3m.primitives.test.SimplePipelineTestPrimitive",
                    "name": "Simple Pipeline Test Primitive"
                  },
                  "arguments": {
                    "inputs": {
                      "type": "CONTAINER",
                      "data": "inputs.0"
                    }
                  },
                  "outputs": [
                    {
                      "id": "produce"
                    }
                  ],
                  "hyperparams": {
                    "columns_to_operate_on": {
                      "type": "CONTAINER",
                      "data": "steps.0.produce"
                    }
                  }
                }
              ]
            }
        """, resolver=Resolver()).check()

    def test_type_check(self):
        with self.assertRaisesRegex(exceptions.InvalidPipelineError, 'Argument \'.*\' of step .* of pipeline \'.*\' has type \'.*\', but it is getting a type \'.*\''):
            pipeline.Pipeline.from_json("""
                {
                  "id": "e8c4dd86-420d-4e1c-ad25-d592a5b5bb0b",
                  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json",
                  "created": "2018-02-28T09:42:27.443844Z",
                  "context": "TESTING",
                  "name": "Test pipeline",
                  "description": "Just a test pipeline",
                  "inputs": [
                    {}
                  ],
                  "outputs": [
                    {
                      "data": "steps.0.produce"
                    }
                  ],
                  "steps": [
                    {
                      "type": "PRIMITIVE",
                      "primitive": {
                        "id": "9c00d42d-382d-4177-a0e7-082da88a29c8",
                        "version": "0.1.0",
                        "python_path": "d3m.primitives.test.SumPrimitive",
                        "name": "Sum Values",
                        "digest": "__SUM_DIGEST__"
                      },
                      "arguments": {
                        "inputs": {
                          "type": "CONTAINER",
                          "data": "inputs.0"
                        }
                      },
                      "outputs": [
                        {
                          "id": "produce"
                        }
                      ]
                    }
                  ]
                }
            """.replace('__SUM_DIGEST__', SumPrimitive.metadata.query()['digest']), resolver=Resolver()).check()


if __name__ == '__main__':
    unittest.main()
