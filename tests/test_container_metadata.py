import collections
import unittest

import numpy
import pandas

from d3m import container
from d3m.metadata import base


class TestContainerMetadata(unittest.TestCase):
    def test_compact_generated_metadata(self):
        ALL_GENERATED_KEYS = ['foo', 'name', 'other', 'structural_type']

        compacted_metadata = base.DataMetadata._compact_generated_metadata({}, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {})

        # All equal.
        new_metadata = {
            ('a',): {'foo': 'bar', 'other': 1},
            ('b',): {'foo': 'bar', 'other': 2},
            ('c',): {'foo': 'bar', 'other': 3},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS,): {'foo': 'bar'},
            ('a',): {'other': 1},
            ('b',): {'other': 2},
            ('c',): {'other': 3},
        })

        # One different.
        new_metadata = {
            ('a',): {'foo': 'bar', 'other': 1},
            ('b',): {'foo': 'bar', 'other': 2},
            ('c',): {'foo': 'bar2', 'other': 3,},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            ('a',): {'foo': 'bar', 'other': 1},
            ('b',): {'foo': 'bar', 'other': 2},
            ('c',): {'foo': 'bar2', 'other': 3,},
        })

        # Recursive.
        new_metadata = {
            ('deep', 'a'): {'foo': 'bar', 'other': 1},
            ('deep', 'b'): {'foo': 'bar', 'other': 2},
            ('deep', 'c'): {'foo': 'bar', 'other': 3},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'a'): {'other': 1},
            (base.ALL_ELEMENTS, 'b'): {'other': 2},
            (base.ALL_ELEMENTS, 'c'): {'other': 3},
        })

        new_metadata = {
            ('deep', 'a'): {'foo': 'bar', 'other': 1},
            ('deep', 'b'): {'foo': 'bar', 'other': 2},
            ('deep', 'c'): {'foo': 'bar', 'other': 3},
            ('deep2', 'd'): {'foo': 'bar', 'other': 4},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'a'): {'other': 1},
            (base.ALL_ELEMENTS, 'b'): {'other': 2},
            (base.ALL_ELEMENTS, 'c'): {'other': 3},
            (base.ALL_ELEMENTS, 'd'): {'other': 4},
        })

        new_metadata = {
            ('deep', 'a'): {'foo': 'bar', 'other': 1},
            ('deep', 'b'): {'foo': 'bar', 'other': 2},
            ('deep', 'c'): {'foo': 'bar', 'other': 3},
            ('deep2', 'a'): {'foo': 'bar', 'other': 4},
            ('deep2', 'b'): {'foo': 'bar', 'other': 5},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'c'): {'other': 3},
            ('deep', 'a'): {'other': 1},
            ('deep', 'b'): {'other': 2},
            ('deep2', 'a'): {'other': 4},
            ('deep2', 'b'): {'other': 5},
        })

        new_metadata = {
            ('deep', 'a'): {'foo': 'bar', 'other': 1},
            ('deep', 'b'): {'foo': 'bar', 'other': 2},
            ('deep', 'c'): {'foo': 'bar2', 'other': 3},
            ('deep2', 'a'): {'foo': 'bar', 'other': 4},
            ('deep2', 'b'): {'foo': 'bar', 'other': 5},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, 'a'): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'b'): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'c'): {'foo': 'bar2', 'other': 3},
            ('deep', 'a'): {'other': 1},
            ('deep', 'b'): {'other': 2},
            ('deep2', 'a'): {'other': 4},
            ('deep2', 'b'): {'other': 5},
        })

        new_metadata = {
            ('a', 'deep'): {'foo': 'bar', 'other': 1},
            ('b', 'deep'): {'foo': 'bar', 'other': 2},
            ('c', 'deep'): {'foo': 'bar2', 'other': 3},
            ('a', 'deep2'): {'foo': 'bar', 'other': 4},
            ('b', 'deep2'): {'foo': 'bar', 'other': 5},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            ('a', base.ALL_ELEMENTS): {'foo': 'bar'},
            ('a', 'deep'): {'other': 1},
            ('a', 'deep2'): {'other': 4},
            ('b', base.ALL_ELEMENTS): {'foo': 'bar'},
            ('b', 'deep'): {'other': 2},
            ('b', 'deep2'): {'other': 5},
            ('c', base.ALL_ELEMENTS): {'foo': 'bar2', 'other': 3},
        })

        new_metadata = {
            (base.ALL_ELEMENTS, 'a'): {'foo': 'bar', 'other': 1},
            (base.ALL_ELEMENTS, 'b'): {'foo': 'bar', 'other': 2},
            (base.ALL_ELEMENTS, 'c'): {'foo': 'bar', 'other': 3},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'a'): {'other': 1},
            (base.ALL_ELEMENTS, 'b'): {'other': 2},
            (base.ALL_ELEMENTS, 'c'): {'other': 3},
        })

        new_metadata = {
            (base.ALL_ELEMENTS, 0): {'foo': 'bar1'},
            (0, 1): {'foo': 'bar2'},
            (1, 1): {'foo': 'bar2'},
            (2, 1): {'foo': 'bar2'},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, 0): {'foo': 'bar1'},
            (base.ALL_ELEMENTS, 1): {'foo': 'bar2'},
        })

        new_metadata = {
            ('deep1', 'a'): {'foo': 'bar', 'other': 1},
            ('deep1', 'b'): {'foo': 'bar2', 'other': 2},
            ('deep2', 'a'): {'foo': 'bar', 'other': 3},
            ('deep2', 'b'): {'foo': 'bar2', 'other': 4},
            ('deep3', 'a'): {'foo': 'bar', 'other': 5},
            ('deep3', 'b'): {'foo': 'bar2', 'other': 6},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, 'a'): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'b'): {'foo': 'bar2'},
            ('deep1', 'a'): {'other': 1},
            ('deep1', 'b'): {'other': 2},
            ('deep2', 'a'): {'other': 3},
            ('deep2', 'b'): {'other': 4},
            ('deep3', 'a'): {'other': 5},
            ('deep3', 'b'): {'other': 6},
        })

        new_metadata = {
            ('deep1', 'a'): {'foo': 'bar', 'other': 1},
            ('deep1', 'b'): {'foo': 'bar', 'other': 2},
            ('deep2', 'c'): {'foo': 'bar', 'other': 3},
            ('deep2', 'd'): {'foo': 'bar', 'other': 4},
            ('deep3', 'e'): {'foo': 'bar', 'other': 5},
            ('deep3', 'f'): {'foo': 'bar', 'other': 6},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS): {'foo': 'bar'},
            (base.ALL_ELEMENTS, 'a'): {'other': 1},
            (base.ALL_ELEMENTS, 'b'): {'other': 2},
            (base.ALL_ELEMENTS, 'c'): {'other': 3},
            (base.ALL_ELEMENTS, 'd'): {'other': 4},
            (base.ALL_ELEMENTS, 'e'): {'other': 5},
            (base.ALL_ELEMENTS, 'f'): {'other': 6},
        })

        new_metadata = {
            ('deep1', 'a', 1): {'foo': 'bar1', 'other': 1},
            ('deep2', 'a', 2): {'foo': 'bar1', 'other': 2},
            ('deep3', 'a', 3): {'foo': 'bar1', 'other': 3},
            ('deep4', 'a', 4): {'foo': 'bar1', 'other': 4},
            ('deep1', 'b', 1): {'foo': 'bar2', 'other': 5},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 2): {'other': 2},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 3): {'other': 3},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 4): {'other': 4},
            (base.ALL_ELEMENTS, 'a', base.ALL_ELEMENTS): {'foo': 'bar1'},
            (base.ALL_ELEMENTS, 'a', 1): {'other': 1},
            (base.ALL_ELEMENTS, 'b', base.ALL_ELEMENTS): {'foo': 'bar2', 'other': 5},
        })

        new_metadata = {
            ('deep', 'a', 1): {'foo': 'bar1', 'other': 1},
            ('deep', 'a', 2): {'foo': 'bar1', 'other': 2},
            ('deep', 'b', 1): {'foo': 'bar2', 'other': 3},
            ('deep', 'b', 2): {'foo': 'bar2', 'other': 4},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, 'a', base.ALL_ELEMENTS): {'foo': 'bar1'},
            (base.ALL_ELEMENTS, 'a', 1): {'other': 1},
            (base.ALL_ELEMENTS, 'a', 2): {'other': 2},
            (base.ALL_ELEMENTS, 'b', base.ALL_ELEMENTS): {'foo': 'bar2'},
            (base.ALL_ELEMENTS, 'b', 1): {'other': 3},
            (base.ALL_ELEMENTS, 'b', 2): {'other': 4},
        })

        new_metadata =  {
            ('deep', 'a', 1): {'foo': 'bar1', 'other': 'bar1'},
            ('deep', 'a', 2): {'foo': 'bar1', 'other': 'bar2'},
            ('deep', 'b', 1): {'foo': 'bar2', 'other': 'bar1'},
            ('deep', 'b', 2): {'foo': 'bar2', 'other': 'bar2'},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 1): {'other': 'bar1'},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 2): {'other': 'bar2'},
            (base.ALL_ELEMENTS, 'a', base.ALL_ELEMENTS): {'foo': 'bar1'},
            (base.ALL_ELEMENTS, 'b', base.ALL_ELEMENTS): {'foo': 'bar2'},
        })

        new_metadata = {
            ('deep1', 'a', 1): {'foo': 'bar1', 'other': 1},
            ('deep1', 'a', 2): {'foo': 'bar1', 'other': 2},
            ('deep2', 'a', 3): {'foo': 'bar1', 'other': 3},
            ('deep2', 'a', 4): {'foo': 'bar1', 'other': 4},
            ('deep1', 'b', 1): {'foo': 'bar2', 'other': 1},
            ('deep1', 'b', 2): {'foo': 'bar2', 'other': 2},
            ('deep2', 'b', 3): {'foo': 'bar2', 'other': 3},
            ('deep2', 'b', 4): {'foo': 'bar2', 'other': 4},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 1): {'other': 1},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 2): {'other': 2},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 3): {'other': 3},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 4): {'other': 4},
            (base.ALL_ELEMENTS, 'a', base.ALL_ELEMENTS): {'foo': 'bar1'},
            (base.ALL_ELEMENTS, 'b', base.ALL_ELEMENTS): {'foo': 'bar2'},
        })

        new_metadata = {
            ('deep', 'a'): {'foo': 'bar', 'other': 1},
            ('deep', 'b'): {'foo': 'bar', 'other': 2},
            ('deep2', 'b'): {'other': 3},
            ('deep2', 'c'): {'foo': 'bar', 'other': 4},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, 'a'): {'other': 1},
            (base.ALL_ELEMENTS, 'c'): {'foo': 'bar', 'other': 4},
            ('deep',base.ALL_ELEMENTS): {'foo': 'bar'},
            ('deep', 'b'): {'other': 2},
            ('deep2', 'b'): {'other': 3},
        })

        new_metadata = {
            ('deep', 'a'): {'foo': 'bar', 'other': 1},
            ('deep', 'b'): {'foo': 'bar', 'other': 2},
            ('deep', 'c'): {'other': 3},
            ('deep2', 'd'): {'foo': 'bar', 'other': 4},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, 'a'): {'foo': 'bar', 'other': 1},
            (base.ALL_ELEMENTS, 'b'): {'foo': 'bar', 'other': 2},
            (base.ALL_ELEMENTS, 'c'): {'other': 3},
            (base.ALL_ELEMENTS, 'd'): {'foo':'bar', 'other': 4},
        })

        new_metadata = {
            (base.ALL_ELEMENTS, 0): {'structural_type': 'numpy.int64'},
            (0, 1): {'structural_type': 'str'},
            (1, 1): {'structural_type': 'str'},
            (2, 1): {'structural_type': 'str'},
            (base.ALL_ELEMENTS, 1): {'name': 'B'},
            (0, 0): {'structural_type': 'numpy.int64'},
            (1, 0): {'structural_type': 'numpy.int64'},
            (2, 0): {'structural_type': 'numpy.int64'},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, 0): {'structural_type': 'numpy.int64'},
            (base.ALL_ELEMENTS, 1): {'name': 'B', 'structural_type': 'str'},
        })

        new_metadata = {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 0): {'structural_type': 'numpy.int64'},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 1): {'structural_type': 'str'},
            ('0', base.ALL_ELEMENTS, 0): {'name': 'A', 'structural_type': 'numpy.int64'},
            ('0', base.ALL_ELEMENTS, 1): {'name': 'B', 'structural_type': 'str'},
        }

        compacted_metadata = base.DataMetadata._compact_generated_metadata(new_metadata, ALL_GENERATED_KEYS)

        self.assertEqual(compacted_metadata, {
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 0): {'structural_type': 'numpy.int64', 'name': 'A'},
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS, 1): {'structural_type': 'str', 'name': 'B'},
        })

    def test_update_with_generated_metadata(self):
        metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.ndarray,
        })

        cells_metadata = collections.OrderedDict()
        cells_metadata[('a',)] = {'other': 1}
        cells_metadata[('b',)] = {'other': 2}
        cells_metadata[('c',)] = {'other': 3}
        cells_metadata[(base.ALL_ELEMENTS,)] = {'foo': 'bar'}
        cells_metadata[('other', 'a')] = {'other': 4}
        cells_metadata[('other', 'b')] = {'other': 5}
        cells_metadata[('other', 'c')] = {'other': 6}
        cells_metadata[('other', base.ALL_ELEMENTS)] = {'foo': 'bar2'}

        metadata._update_with_generated_metadata(cells_metadata)

        self.assertEqual(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'foo': 'bar'},
        }, {
            'selector': ['a'],
            'metadata': {'other': 1},
        }, {
            'selector': ['b'],
            'metadata': {'other': 2},
        }, {
            'selector': ['c'],
            'metadata': {'other': 3},
        }, {
            'selector': ['other', '__ALL_ELEMENTS__'],
            'metadata': {'foo': 'bar2'},
        }, {
            'selector': ['other', 'a'],
            'metadata': {'other': 4},
        }, {
            'selector': ['other', 'b'],
            'metadata': {'other': 5},
        }, {
            'selector': ['other', 'c'],
            'metadata': {'other': 6},
        }])

        metadata = base.DataMetadata({
            'schema': base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.ndarray,
            'semantic_types': ['http://example.com/Type1'],
            'dimension': {
                'length': 0,
                'foobar': 42,
                'semantic_types': ['http://example.com/Type2'],
            }
        })

        metadata = metadata.update(('a',), {
            'semantic_types': ['http://example.com/Type3'],
            'dimension': {
                'length': 0,
                'foobar': 45,
                'semantic_types': ['http://example.com/Type4'],
            }
        })

        cells_metadata = collections.OrderedDict()
        cells_metadata[()] = {
            'other': 1,
            'structural_type': container.ndarray,
            'semantic_types': ['http://example.com/Type1a'],
            'dimension': {
                'length': 100,
                'name': 'test1',
                'semantic_types': ['http://example.com/Type2a'],
            }
        }
        cells_metadata[('a',)] = {
            'semantic_types': ['http://example.com/Type3', 'http://example.com/Type3a'],
            'dimension': {
                'length': 200,
                'name': 'test2',
                'semantic_types': ['http://example.com/Type4', 'http://example.com/Type4a'],
            }
        }
        cells_metadata[('b',)] = {'other': 2}

        metadata._update_with_generated_metadata(cells_metadata)

        self.assertEqual(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
                'other': 1,
                'semantic_types': ['http://example.com/Type1', 'http://example.com/Type1a'],
                'dimension': {
                    'length': 100,
                    'name': 'test1',
                    'foobar': 42,
                    'semantic_types': ['http://example.com/Type2', 'http://example.com/Type2a'],
                },
            },
        }, {
            'selector': ['a'],
            'metadata': {
                'semantic_types': ['http://example.com/Type3', 'http://example.com/Type3a'],
                'dimension': {
                    'length': 200,
                    'name': 'test2',
                    'foobar': 45,
                    'semantic_types': ['http://example.com/Type4', 'http://example.com/Type4a'],
                },
            },
        }, {
            'selector': ['b'],
            'metadata': {'other': 2},
        }])

    def test_dataframe(self):
        df = container.DataFrame({'A': [1, 2, 3], 'B': ['a', 'b', 'c']})

        self.assertEqual(df.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
                'structural_type': 'numpy.int64',
            }
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
                'structural_type': 'str',
            },
        }])

    def test_greedy_prune_metadata(self):
        # Warmup test 1.
        selectors_to_compact = [('a',), ('b',),('c',)]
        compacted_selector = [(base.ALL_ELEMENTS, )]

        pruned_selectors = base.DataMetadata._greedy_prune_selector(compacted_selector, selectors_to_compact)

        self.assertEqual(pruned_selectors, [
            (base.ALL_ELEMENTS, )
        ])

        # Warmup test 2.
        selectors_to_compact = [('deep', 'a'), ('deep', 'b'), ('deep', 'c'), ('deep2', 'd')]
        compacted_selector = [(base.ALL_ELEMENTS, base.ALL_ELEMENTS,)]

        pruned_selectors = base.DataMetadata._greedy_prune_selector(compacted_selector, selectors_to_compact)

        self.assertEqual(pruned_selectors, [
            (base.ALL_ELEMENTS, base.ALL_ELEMENTS,)
        ])

        # Check if it can remove unnecessary outputs.
        selectors_to_compact = [('deep', 'a'), ('deep', 'b'), ('deep2', 'a'), ('deep2', 'b')]
        compacted_selector = [(base.ALL_ELEMENTS, 'a'), (base.ALL_ELEMENTS, 'b'), ('deep', 'a'), ('deep2', 'b')]

        pruned_selectors = base.DataMetadata._greedy_prune_selector(compacted_selector, selectors_to_compact)

        self.assertEqual(pruned_selectors, [
            (base.ALL_ELEMENTS, 'a'), (base.ALL_ELEMENTS, 'b')
        ])

        # Case when compacted_selector overlaps.
        selectors_to_compact = [('a', 'deep'), ('b', 'deep'), ('a', 'deep2'), ('b', 'deep2')]
        compacted_selector = [('a', base.ALL_ELEMENTS), ('b', base.ALL_ELEMENTS), (base.ALL_ELEMENTS, 'deep2')]

        pruned_selectors = base.DataMetadata._greedy_prune_selector(compacted_selector, selectors_to_compact)

        self.assertEqual(pruned_selectors, [
            ('a', base.ALL_ELEMENTS), ('b', base.ALL_ELEMENTS)
        ])

        # Check the order.
        selectors_to_compact = [('a', 'deep'), ('b', 'deep'), ('a', 'deep2'), ('b', 'deep2')]
        compacted_selector = [(base.ALL_ELEMENTS, 'deep2'), ('a', base.ALL_ELEMENTS), ('b', base.ALL_ELEMENTS),]

        pruned_selectors = base.DataMetadata._greedy_prune_selector(compacted_selector, selectors_to_compact)

        self.assertEqual(pruned_selectors, [
            ('a', base.ALL_ELEMENTS), ('b', base.ALL_ELEMENTS)
        ])

        # More complex compacted_selectors.
        selectors_to_compact = [('a', 'deep'), ('b', 'deep'), ('a', 'deep2'), ('b', 'deep2')]
        compacted_selector = [(base.ALL_ELEMENTS, 'deep2'), ('a', base.ALL_ELEMENTS),
                              (base.ALL_ELEMENTS, 'deep'), ('b', base.ALL_ELEMENTS),]

        pruned_selectors = base.DataMetadata._greedy_prune_selector(compacted_selector, selectors_to_compact)

        self.assertEqual(pruned_selectors, [
            (base.ALL_ELEMENTS, 'deep2'), (base.ALL_ELEMENTS, 'deep')
        ])

        # All-elements in selectors_to_compact.
        selectors_to_compact = [('deep', 'a', 1), ('deep', base.ALL_ELEMENTS, 2)]
        compacted_selector = [(base.ALL_ELEMENTS, 'a', base.ALL_ELEMENTS), ('deep', base.ALL_ELEMENTS, 2)]

        pruned_selectors = base.DataMetadata._greedy_prune_selector(compacted_selector, selectors_to_compact)

        self.assertEqual(pruned_selectors, [
            (base.ALL_ELEMENTS, 'a', base.ALL_ELEMENTS), ('deep', base.ALL_ELEMENTS, 2)
        ])

    def test_semantic_types_merge(self):
        metadata = base.DataMetadata().update(('0',), {
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint'],
        })

        metadata_regular = metadata.update((base.ALL_ELEMENTS,), {
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
        })

        self.assertEqual(metadata_regular.query(('0',)).get('semantic_types', None), ('https://metadata.datadrivendiscovery.org/types/Table',))

        metadata._update_with_generated_metadata({
            (base.ALL_ELEMENTS,): {
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
            },
        })

        self.assertEqual(metadata.query(('0',)).get('semantic_types', None), ('https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint', 'https://metadata.datadrivendiscovery.org/types/Table',))

    def test_dataset(self):
        dataset = container.Dataset({'0': container.DataFrame({'A': [1, 2, 3], 'B': ['a', 'b', 'c']})})

        self.assertEqual(dataset.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.dataset.Dataset',
                'dimension': {
                    'name': 'resources',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
                    'length': 1,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
                'structural_type': 'str',
            },
        }])

    def test_list(self):
        lst = container.List(['a', 'b', 'c'])

        self.assertEqual(lst.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.list.List',
                'dimension': {
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'str',
            },
        }])

        lst = container.List([1, 'a', 2.0])

        self.assertEqual(lst.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.list.List',
                'dimension': {
                    'length': 3,
                },
            },
        }, {
            'selector': [0],
            'metadata': {
                'structural_type': 'int',
            },
        }, {
            'selector': [1],
            'metadata': {
                'structural_type': 'str',
            },
        }, {
            'selector': [2],
            'metadata': {
                'structural_type': 'float',
            },
        }])

        lst = container.List([container.DataFrame({'A': [1, 2, 3], 'B': ['a', 'b', 'c']})])

        self.assertEqual(lst.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.list.List',
                'dimension': {
                    'length': 1,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
                'structural_type': 'str',
            },
        }])

    def test_ndarray(self):
        array = container.ndarray(numpy.array([1, 2, 3]))

        self.assertEqual(array.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
                'dimension': {
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])

    def test_matrix(self):
        matrix = container.matrix(numpy.array([[1, 2, 3], [4, 5, 6]]))

        self.assertEqual(matrix.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'structural_type': 'd3m.container.numpy.matrix',
                'dimension': {
                    'length': 2,
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 3,
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])

    def test_dataframe_with_names_kept(self):
        df = container.DataFrame({'A': [1, 2, 3], 'B': ['a', 'b', 'c']})

        df.metadata = df.metadata.update((base.ALL_ELEMENTS, 0), {
            'name': 'first_column',
        })
        df.metadata = df.metadata.update((base.ALL_ELEMENTS, 1), {
            'name': 'second_column',
        })

        self.assertEqual(df.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'first_column',
                'structural_type': 'numpy.int64',
            }
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'second_column',
                'structural_type': 'str',
            },
        }])

        df2 = container.DataFrame({'A': [1, 2, 3, 4], 'B': ['a', 'b', 'c', 'd']})

        df2.metadata = df.metadata.set_for_value(df2)

        self.assertEqual(df2.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 4,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'first_column',
                'structural_type': 'numpy.int64',
            }
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'second_column',
                'structural_type': 'str',
            },
        }])

    def test_dataframe_tabular_semantic_types(self):
        # A DataFrame with explicit WRONG metadata.
        df = container.DataFrame({'A': [1, 2, 3], 'B': ['a', 'b', 'c']}, {
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
            },
        })

        self.assertEqual(df.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    # We respect the name, but we override the semantic types.
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
                'structural_type': 'numpy.int64',
            }
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
                'structural_type': 'str',
            },
        }])

    def test_complex_value(self):
        dataset = container.Dataset({
            '0': container.DataFrame({
                'A': [
                    container.ndarray(numpy.array(['a', 'b', 'c'])),
                    container.ndarray(numpy.array([1, 2, 3])),
                    container.ndarray(numpy.array([1.0, 2.0, 3.0])),
                ],
                'B': [
                    container.List(['a', 'b', 'c']),
                    container.List([1, 2, 3]),
                    container.List([1.0, 2.0, 3.0]),
                ],
            }),
        })

        self.assertEqual(dataset.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.dataset.Dataset',
                'dimension': {
                    'name': 'resources',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
                    'length': 1,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 2,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 3
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 0],
            'metadata': {
                'structural_type': 'd3m.container.numpy.ndarray',
                'name': 'A',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 1],
            'metadata': {
                'structural_type': 'd3m.container.list.List',
                'name': 'B',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0, 0, '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.str_',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0, 1, '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'str',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1, 0, '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1, 1, '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'int',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2, 0, '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.float64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2, 1, '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'float',
            }
        }])

    def test_large_dataframe_with_objects(self):
        df = pandas.DataFrame({str(i): [str(j) for j in range(10000)] for i in range(50)}, columns=[str(i) for i in range(50)])

        df = container.DataFrame(df)

        column_names = [{'selector': ['__ALL_ELEMENTS__', i], 'metadata': {'name': str(i)}} for i in range(50)]

        self.assertEqual(df.metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'schema': base.CONTAINER_SCHEMA_VERSION,
                    'structural_type': 'd3m.container.pandas.DataFrame',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                    'dimension': {
                        'name': 'rows',
                        'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                        'length': 10000,
                    },
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__'],
                'metadata': {
                    'dimension': {
                        'name': 'columns',
                        'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                        'length': 50,
                    },
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
                'metadata': {
                    'structural_type': 'str',
                },
            }
        ] + column_names)

    def test_large_list_with_objects(self):
        l = container.List([container.List([str(j) for i in range(50)], generate_metadata=False) for j in range(10000)])

        self.assertEqual(l.metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'schema': base.CONTAINER_SCHEMA_VERSION,
                    'structural_type': 'd3m.container.list.List',
                    'dimension': {
                        'length': 10000,
                    },
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__'],
                'metadata': {
                    'structural_type': 'd3m.container.list.List',
                    'dimension': {
                        'length': 50,
                    },
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
                'metadata': {
                    'structural_type': 'str',
                },
            },
        ])

    def test_large_ndarray_with_objects(self):
        array = numpy.array([[[str(k) for k in range(5)] for i in range(10)] for j in range(10000)], dtype=object)

        array = container.ndarray(array)

        self.assertEqual(array.metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'schema': base.CONTAINER_SCHEMA_VERSION,
                    'structural_type': 'd3m.container.numpy.ndarray',
                    'dimension': {
                        'length': 10000,
                    },
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__'],
                'metadata': {
                    'dimension': {
                        'length': 10,
                    },
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
                'metadata': {
                    'dimension': {
                        'length': 5,
                    },
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
                'metadata': {
                    'structural_type': 'str',
                },
            },
        ])

    def test_large_dict_with_objects(self):
        l = container.List([{str(i): {str(j): j for j in range(10000)} for i in range(50)}])

        self.assertEqual(l.metadata.to_json_structure(), [
            {
                'selector': [],
                'metadata': {
                    'schema': base.CONTAINER_SCHEMA_VERSION,
                    'structural_type': 'd3m.container.list.List',
                    'dimension': {
                        'length': 1,
                    },
                },
            },
            {
                'selector': ['__ALL_ELEMENTS__'],
                'metadata': {
                    'structural_type': 'dict',
                    'dimension': {
                        'length': 50,
                    },
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
                'metadata': {
                    'structural_type': 'dict',
                    'dimension': {
                        'length': 10000.
                    },
                }
            },
            {
                'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
                'metadata': {
                    'structural_type': 'int',
                },
            },
        ])


if __name__ == '__main__':
    unittest.main()
