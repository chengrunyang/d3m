import copy
import pickle
import unittest

import numpy
import pandas

from d3m import container
from d3m.metadata import base as metadata_base

copy_functions = {
    'obj.copy()': lambda obj: obj.copy(),
    'obj[:]': lambda obj: obj[:],
    'copy.copy()': lambda obj: copy.copy(obj),
    'copy.deepcopy()': lambda obj: copy.deepcopy(obj),
    'pickle.loads(pickle.dumps())': lambda obj: pickle.loads(pickle.dumps(obj)),
}


class TestContainers(unittest.TestCase):
    def test_list(self):
        l = container.List()

        self.assertTrue(hasattr(l, 'metadata'))

        l = container.List([1, 2, 3])

        l.metadata = l.metadata.update((), {
            'test': 'foobar',
        })

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertIsInstance(l, container.List)
        self.assertTrue(hasattr(l, 'metadata'))
        self.assertIs(l.metadata.for_value, l)
        self.assertEqual(l.metadata.query(()).get('test'), 'foobar')

        self.assertIsInstance(l, container.List)
        self.assertIsInstance(l, list)

        self.assertNotIsInstance([], container.List)

        for name, copy_function in copy_functions.items():
            l_copy = copy_function(l)

            self.assertIsInstance(l_copy, container.List, name)
            self.assertTrue(hasattr(l_copy, 'metadata'), name)
            self.assertIs(l_copy.metadata.for_value, l_copy, name)

            self.assertSequenceEqual(l, l_copy, name)
            self.assertEqual(l.metadata.to_json_structure(), l_copy.metadata.to_json_structure(), name)
            self.assertEqual(l_copy.metadata.query(()).get('test'), 'foobar', name)

        l_copy = container.List(l, {
            'test2': 'barfoo',
        })

        self.assertIsInstance(l_copy, container.List)
        self.assertTrue(hasattr(l_copy, 'metadata'))
        self.assertIs(l_copy.metadata.for_value, l_copy)

        self.assertSequenceEqual(l, l_copy)
        self.assertEqual(l_copy.metadata.query(()), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.List,
            'dimension': {
                'length': 3,
            },
            'test': 'foobar',
            'test2': 'barfoo',
        })

        self.assertEqual(l[1], 2)

        with self.assertRaisesRegex(TypeError, 'list indices must be integers or slices, not tuple'):
            l[1, 2]

        l_slice = l[1:3]

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_slice, [2, 3])
        self.assertIsInstance(l_slice, container.List)
        self.assertTrue(hasattr(l_slice, 'metadata'))
        self.assertIs(l_slice.metadata.for_value, l_slice)
        self.assertEqual(l.metadata.to_json_structure(), l_slice.metadata.to_json_structure())

        l_added = l + [4, 5]

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_added, [1, 2, 3, 4, 5])
        self.assertIsInstance(l_added, container.List)
        self.assertTrue(hasattr(l_added, 'metadata'))
        self.assertIs(l_added.metadata.for_value, l_added)
        self.assertEqual(l.metadata.to_json_structure(), l_added.metadata.to_json_structure())

        l_added += [6, 7]

        self.assertSequenceEqual(l_added, [1, 2, 3, 4, 5, 6, 7])
        self.assertIsInstance(l_added, container.List)
        self.assertTrue(hasattr(l_added, 'metadata'))
        self.assertIs(l_added.metadata.for_value, l_added)
        self.assertEqual(l.metadata.to_json_structure(), l_added.metadata.to_json_structure())

        l_multiplied = l * 3

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_multiplied, [1, 2, 3, 1, 2, 3, 1, 2, 3])
        self.assertIsInstance(l_multiplied, container.List)
        self.assertTrue(hasattr(l_multiplied, 'metadata'))
        self.assertIs(l_multiplied.metadata.for_value, l_multiplied)
        self.assertEqual(l.metadata.to_json_structure(), l_multiplied.metadata.to_json_structure())

        l_multiplied = 3 * l

        self.assertSequenceEqual(l, [1, 2, 3])
        self.assertSequenceEqual(l_multiplied, [1, 2, 3, 1, 2, 3, 1, 2, 3])
        self.assertIsInstance(l_multiplied, container.List)
        self.assertTrue(hasattr(l_multiplied, 'metadata'))
        self.assertIs(l_multiplied.metadata.for_value, l_multiplied)
        self.assertEqual(l.metadata.to_json_structure(), l_multiplied.metadata.to_json_structure())

        l_multiplied *= 2

        self.assertSequenceEqual(l_multiplied, [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3])
        self.assertIsInstance(l_multiplied, container.List)
        self.assertTrue(hasattr(l_multiplied, 'metadata'))
        self.assertIs(l_multiplied.metadata.for_value, l_multiplied)
        self.assertEqual(l.metadata.to_json_structure(), l_multiplied.metadata.to_json_structure())

    def test_ndarray(self):
        array = container.ndarray(numpy.array([1, 2, 3]))
        self.assertTrue(numpy.array_equal(array, [1, 2, 3]))
        self.assertIsInstance(array, container.ndarray)
        self.assertTrue(hasattr(array, 'metadata'))
        self.assertIs(array.metadata.for_value, array)

        self.assertIsInstance(array, numpy.ndarray)

        self.assertNotIsInstance(numpy.array([]), container.ndarray)

        array.metadata = array.metadata.update((), {
            'test': 'foobar',
        })

        self.assertEqual(array.metadata.query(()).get('test'), 'foobar')

        for name, copy_function in copy_functions.items():
            array_copy = copy_function(array)

            self.assertIsInstance(array_copy, container.ndarray, name)
            self.assertTrue(hasattr(array_copy, 'metadata'), name)
            self.assertIs(array_copy.metadata.for_value, array_copy, name)

            self.assertTrue(numpy.array_equal(array, array_copy), name)
            self.assertEqual(array.metadata.to_json_structure(), array_copy.metadata.to_json_structure(), name)
            self.assertEqual(array_copy.metadata.query(()).get('test'), 'foobar', name)


        array_copy = container.ndarray(array, {
            'test2': 'barfoo',
        })

        self.assertIsInstance(array_copy, container.ndarray)
        self.assertTrue(hasattr(array_copy, 'metadata'))
        self.assertIs(array_copy.metadata.for_value, array_copy)

        self.assertTrue(numpy.array_equal(array, array_copy))
        self.assertEqual(array_copy.metadata.query(()), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.ndarray,
            'dimension': {
                'length': 3,
            },
            'test': 'foobar',
            'test2': 'barfoo',
        })

        array_from_list = container.ndarray([1, 2, 3])
        self.assertTrue(numpy.array_equal(array_from_list, [1, 2, 3]))
        self.assertIsInstance(array_from_list, container.ndarray)
        self.assertTrue(hasattr(array_from_list, 'metadata'))
        self.assertIs(array_from_list.metadata.for_value, array_from_list)

    def test_matrix(self):
        matrix = container.matrix(numpy.array([[1, 2], [3, 4]]))
        self.assertTrue(numpy.array_equal(matrix, [[1, 2], [3, 4]]))
        self.assertIsInstance(matrix, container.matrix)
        self.assertTrue(hasattr(matrix, 'metadata'))
        self.assertIs(matrix.metadata.for_value, matrix)

        self.assertIsInstance(matrix, numpy.ndarray)
        self.assertIsInstance(matrix, numpy.matrix)
        self.assertIsInstance(matrix, container.ndarray)

        self.assertNotIsInstance(numpy.matrix([]), container.matrix)

        matrix.metadata = matrix.metadata.update((), {
            'test': 'foobar',
        })

        self.assertEqual(matrix.metadata.query(()).get('test'), 'foobar')

        for name, copy_function in copy_functions.items():
            matrix_copy = copy_function(matrix)

            self.assertIsInstance(matrix_copy, container.matrix, name)
            self.assertTrue(hasattr(matrix_copy, 'metadata'), name)
            self.assertIs(matrix_copy.metadata.for_value, matrix_copy, name)

            self.assertTrue(numpy.array_equal(matrix, matrix_copy), name)
            self.assertEqual(matrix.metadata.to_json_structure(), matrix_copy.metadata.to_json_structure(), name)
            self.assertEqual(matrix_copy.metadata.query(()).get('test'), 'foobar', name)

        matrix_copy = container.matrix(matrix, {
            'test2': 'barfoo',
        })

        self.assertIsInstance(matrix_copy, container.matrix)
        self.assertTrue(hasattr(matrix_copy, 'metadata'))
        self.assertIs(matrix_copy.metadata.for_value, matrix_copy)

        self.assertTrue(numpy.array_equal(matrix, matrix_copy))
        self.assertEqual(matrix_copy.metadata.query(()), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.matrix,
            'semantic_types': ('https://metadata.datadrivendiscovery.org/types/Table',),
            'dimension': {
                'name': 'rows',
                'semantic_types': ('https://metadata.datadrivendiscovery.org/types/TabularRow',),
                'length': 2
            },
            'test': 'foobar',
            'test2': 'barfoo',
        })

        matrix_from_list = container.matrix([[1, 2], [3, 4]])
        self.assertTrue(numpy.array_equal(matrix_from_list, [[1, 2], [3, 4]]))
        self.assertIsInstance(matrix_from_list, container.matrix)
        self.assertTrue(hasattr(matrix_from_list, 'metadata'))
        self.assertIs(matrix_from_list.metadata.for_value, matrix_from_list)

    def test_dataframe(self):
        df = container.DataFrame(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}))
        self.assertTrue(df._data.equals(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})._data))
        self.assertIsInstance(df, container.DataFrame)
        self.assertTrue(hasattr(df, 'metadata'))
        self.assertIs(df.metadata.for_value, df)

        self.assertIsInstance(df, pandas.DataFrame)

        self.assertNotIsInstance(pandas.DataFrame({'A': [1, 2, 3]}), container.DataFrame)

        df.metadata = df.metadata.update((), {
            'test': 'foobar',
        })

        self.assertEqual(df.metadata.query(()).get('test'), 'foobar')

        for name, copy_function in copy_functions.items():
            df_copy = copy_function(df)

            self.assertIsInstance(df_copy, container.DataFrame, name)
            self.assertTrue(hasattr(df_copy, 'metadata'), name)
            self.assertIs(df_copy.metadata.for_value, df_copy, name)

            self.assertTrue(df.equals(df_copy), name)
            self.assertEqual(df.metadata.to_json_structure(), df_copy.metadata.to_json_structure(), name)
            self.assertEqual(df_copy.metadata.query(()).get('test'), 'foobar', name)

        df_copy = container.DataFrame(df, {
            'test2': 'barfoo',
        })

        self.assertIsInstance(df_copy, container.DataFrame)
        self.assertTrue(hasattr(df_copy, 'metadata'))
        self.assertIs(df_copy.metadata.for_value, df_copy)

        self.assertTrue(numpy.array_equal(df, df_copy))
        self.assertEqual(df_copy.metadata.query(()), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
            'semantic_types': ('https://metadata.datadrivendiscovery.org/types/Table',),
            'dimension': {
                'name': 'rows',
                'semantic_types': ('https://metadata.datadrivendiscovery.org/types/TabularRow',),
                'length': 3
            },
            'test': 'foobar',
            'test2': 'barfoo',
        })

        df_from_dict = container.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})
        self.assertTrue(df_from_dict._data.equals(pandas.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]})._data))
        self.assertIsInstance(df_from_dict, container.DataFrame)
        self.assertTrue(hasattr(df_from_dict, 'metadata'))
        self.assertIs(df_from_dict.metadata.for_value, df_from_dict)

    def test_dataset(self):
        dataset = container.Dataset.load('sklearn://boston')

        self.assertIsInstance(dataset, container.Dataset)
        self.assertTrue(hasattr(dataset, 'metadata'))
        self.assertIs(dataset.metadata.for_value, dataset)

        dataset.metadata = dataset.metadata.update((), {
            'test': 'foobar',
        })

        self.assertEqual(dataset.metadata.query(()).get('test'), 'foobar')

        for name, copy_function in copy_functions.items():
            # Not supported on dicts.
            if name == 'obj[:]':
                continue

            dataset_copy = copy_function(dataset)

            self.assertIsInstance(dataset_copy, container.Dataset, name)
            self.assertTrue(hasattr(dataset_copy, 'metadata'), name)
            self.assertIs(dataset_copy.metadata.for_value, dataset_copy, name)

            self.assertEqual(len(dataset), len(dataset_copy), name)
            self.assertEqual(dataset.keys(), dataset_copy.keys(), name)
            for resource_name in dataset.keys():
                self.assertTrue(numpy.array_equal(dataset[resource_name], dataset_copy[resource_name]), name)
            self.assertEqual(dataset.metadata.to_json_structure(), dataset_copy.metadata.to_json_structure(), name)
            self.assertEqual(dataset_copy.metadata.query(()).get('test'), 'foobar', name)

    def test_list_ndarray_int(self):
        # With custom metadata which should be preserved.
        l = container.List([1, 2, 3], {
            'foo': 'bar',
        })

        self.assertEqual(l.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.list.List',
                'dimension': {
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'int',
            },
        }])

        array = container.ndarray(l)

        self.assertEqual(array.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
                'dimension': {
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])

        l2 = container.List(array)

        self.assertEqual(l2.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.list.List',
                'dimension': {
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])

    def test_dataframe_ndarray_int(self):
        # With custom metadata which should be preserved.
        df = container.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}, {
            'foo': 'bar',
        })

        self.assertEqual(df.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'C',
            },
        }])

        array = container.ndarray(df)

        self.assertEqual(array.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'C',
            },
        }])

        df2 = container.DataFrame(array)

        self.assertEqual(df2.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'C',
            },
        }])

    def test_dataframe_list_int(self):
        # With custom metadata which should be preserved.
        df = container.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}, {
            'foo': 'bar',
        })

        self.assertEqual(df.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'C',
            },
        }])

        l = container.List(df)

        self.assertEqual(l.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.list.List',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'd3m.container.list.List',
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'int',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'C',
            },
        }])

        df2 = container.DataFrame(l)

        self.assertEqual(df2.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'dimension': {
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'A',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 1],
            'metadata': {
                'name': 'B',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', 2],
            'metadata': {
                'name': 'C',
            },
        }])

    def test_deep_ndarray(self):
        # With custom metadata which should be preserved.
        array = container.ndarray(numpy.arange(3 * 4 * 5 * 5 * 5).reshape((3, 4, 5, 5, 5)), {
            'foo': 'bar',
        })

        self.assertEqual(array.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
                'dimension': {
                    'length': 3,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 4,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])

        df = container.DataFrame(array)

        self.assertEqual(df.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'foo': 'bar',
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.pandas.DataFrame',
                'dimension': {
                    'length': 3,
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                },
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 4,
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
                'structural_type': 'd3m.container.numpy.ndarray',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])

        array2 = container.ndarray(df)

        # We do not automatically compact numpy with nested numpy arrays into one array
        # (there might be an exception if array is jagged).
        self.assertEqual(array2.metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': 'd3m.container.numpy.ndarray',
                'dimension': {
                    'length': 3,
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                },
                'foo': 'bar',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
            },
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 4,
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
                'structural_type': 'd3m.container.numpy.ndarray',
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 5,
                },
            },
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {
                'structural_type': 'numpy.int64',
            },
        }])


if __name__ == '__main__':
    unittest.main()
