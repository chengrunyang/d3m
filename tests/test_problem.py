import os.path
import unittest

from d3m.metadata import problem


class TestProblem(unittest.TestCase):
    def test_basic(self):
        problem_doc_path = os.path.join(os.path.dirname(__file__), 'data', 'problems', 'iris_problem_1', 'problemDoc.json')

        self.assertEqual(problem.parse_problem_description(problem_doc_path), {
            'schema': problem.PROBLEM_SCHEMA_VERSION,
            'problem': {
                'id': 'iris_problem_1',
                'version': '1.0',
                'name': 'Distinguish Iris flowers',
                'description': 'Distinguish Iris flowers of three related species.',
                'task_type': problem.TaskType.CLASSIFICATION,
                'task_subtype': problem.TaskSubtype.MULTICLASS,
                'performance_metrics': [
                    {
                        'metric': problem.PerformanceMetric.ACCURACY,
                        'params': {},
                    }
                ]
            },
            'inputs': [
                {
                    'dataset_id': 'iris_dataset_1',
                    'targets': [
                        {
                            'target_index': 0,
                            'resource_id': '0',
                            'column_index': 5,
                            'column_name': 'species',
                        }
                    ]
                }
            ],
            'outputs': {
                'predictions_file': 'predictions.csv',
                'scores_file': 'scores.csv'
            }
        })

    def test_unparse(self):
        self.assertEqual(problem.TaskType.CLASSIFICATION.unparse(), 'classification')
        self.assertEqual(problem.TaskSubtype.MULTICLASS.unparse(), 'multiClass')
        self.assertEqual(problem.PerformanceMetric.ACCURACY.unparse(), 'accuracy')


if __name__ == '__main__':
    unittest.main()
